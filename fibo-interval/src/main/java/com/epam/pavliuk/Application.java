package com.epam.pavliuk;

/**
 * Main class.
 */
public class Application {

  /**
   * main starting point.
   * @param args
   */
  public static void main(String[] args) {
    FiboInterval fiboInterval = new FiboInterval();
    System.out.println("Enter left and right bounds of interval:");
    
    int leftBound = fiboInterval.scanNumber();
    int rightBound = fiboInterval.scanNumber();
    System.out.println("Your interval is [" + leftBound + "," + rightBound + "]");
    fiboInterval.printOddNumbersFromStartToEndOfInterval(leftBound, rightBound);
    fiboInterval.printEvenNumbersFromEndToStartOfInterval(leftBound, rightBound);
    int maxOddNum = fiboInterval.getMaxOddNumFromInterval(rightBound);
    int maxEvenNum = fiboInterval.getMaxEvenNumFromInterval(rightBound);
    fiboInterval.buildFibonnaciNumbers(maxOddNum, maxEvenNum);
  }
}