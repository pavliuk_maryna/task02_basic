package com.epam.pavliuk;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Main fibo interval calculation class.
 */
public class FiboInterval {

  /**
   * gets max odd num from interval.
   * @param number
   * @return
   */
  public int getMaxOddNumFromInterval(int number) {
    while (true) {
      number--;
      if (isOdd(number)) {
        return number;
      }
    }
  }

  /**
   * gets max even num from interval.
   * @param number
   * @return
   */
  public int getMaxEvenNumFromInterval(int number) {
    while (true) {
      number--;
      if (!isOdd(number)) {
        return number;
      }
    }
  }

  /**
   * builds fibo sequence.
   * @param f1
   * @param f2
   */
  public void buildFibonnaciNumbers(int f1, int f2) {
    System.out.println("Enter size Of Set FiboNumbers:");
    int left = f1;
    int right = f2;
    ArrayList<Integer> fiboNumbers = new ArrayList<>();
    fiboNumbers.add(left);
    fiboNumbers.add(right);
    int fibo;
    int sizeOfSetFiboNumbers = scanNumber();
    int quantityOfFiboNumbers = 2;
    do {
      fibo = left + right;
      left = right;
      right = fibo;
      fiboNumbers.add(fibo);
      quantityOfFiboNumbers++;
    } while (sizeOfSetFiboNumbers != quantityOfFiboNumbers);
    System.out.println(fiboNumbers);
    int quantityOfOddNumbers = 0;
    int quantityOfEvenNumbers = 0;
    for (int k = 0; k < sizeOfSetFiboNumbers; k++) {
      if (isOdd(fiboNumbers.get(k))) {
        quantityOfOddNumbers++;
      } else {
        quantityOfEvenNumbers++;
      }
    }
    System.out.println(
        "quantity of Odd Numbers is:" + quantityOfOddNumbers + ", quantity of Even Numbers is: "
            + quantityOfEvenNumbers);
  }

  /**
   * scans numbers.
   * @return int
   */
  public int scanNumber() {
    Scanner scan = new Scanner(System.in);
    return scan.nextInt();
  }

  /**
   * prints numbers.
   * @param firstNumberOfInterval
   * @param lastNumberOfInterval
   */
  public void printOddNumbersFromStartToEndOfInterval(int firstNumberOfInterval,
      int lastNumberOfInterval) {
    int sum = 0;
    for (int i = firstNumberOfInterval; i <= lastNumberOfInterval; i++) {
      if (isOdd(i)) {
        sum += i;
        System.out.print(i + " ");
      }
    }
    System.out.println("Sum of odd numbers is: " + sum);
  }

  /**
   * prints numbers.
   * @param firstNumberOfInterval
   * @param lastNumberOfInterval
   */
  public void printEvenNumbersFromEndToStartOfInterval(int firstNumberOfInterval,
      int lastNumberOfInterval) {
    int sum = 0;
    for (int i = lastNumberOfInterval; i >= firstNumberOfInterval; i--) {
      if (!isOdd(i)) {
        sum += i;
        System.out.print(i + " ");
      }
    }
    System.out.println("Sum of even numbers is: " + sum);
  }

  /**
   * checks whether the num is odd.
   * @param num
   * @return
   */
  private boolean isOdd(int num) {
    return num % 2 != 0;
  }

}